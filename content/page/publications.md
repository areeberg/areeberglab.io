---
title: Publications
subtitle: ...
comments: false
---
### Journal articles

- **Support vector candidates selection via Delaunay graph and convex-hull for large and high-dimensional datasets.** Reeberg de Mello, Alexandre ; Stemmer, Marcelo Ricardo; Gabriel Oliveira Barbosa, Flávio . ., 2018, Pattern Recognition Letters, Volume 116, 1 December 2018, Pages 43-49. (https://www.sciencedirect.com/science/article/abs/pii/S0167865518305592)

### Book chapter

- **Automated Visual Inspection System for Printed Circuit Boards for Small Series Production: A Multiagent Context Approach.** Reeberg de Mello, Alexandre ; Stemmer, Marcelo Ricardo . . In: Oleg Sergiyenko; Julio C. Rodríguez-Quiñonez. (Org.). 1 ed. : IGI Global, 2016, v. , p. 79-107. (https://www.igi-global.com/chapter/automated-visual-inspection-system-for-printed-circuit-boards-for-small-series-production/161988)

### Conference proceedings

- **Inspecting surface mounted devices using k nearest neighbor and Multilayer Perceptron.** DE MELLO, ALEXANDRE R. ; STEMMER, MARCELO R. . , 2015, Buzios. 2015 IEEE 24th International Symposium on Industrial Electronics (ISIE), 2015. p. 950.  (https://ieeexplore.ieee.org/document/7281599)