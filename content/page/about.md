---
title: About me
subtitle: Academic formation
comments: false
---

MEng. Alexandre Reeberg de Mello:

- PhD Candidate @Federal University of Santa Catarina (UFSC)
- Masters in Automation and Systems @Federal University of Santa Catarina (UFSC)
- Undergrad in Control and Automation @Pontifícia Universidade Católica do Paraná (PUC-PR)

### S2i

S2i (stands for Industrial Intelligent Systems) is a research group from UFSC, which belongs to Automation and Systems Engineering Graduate Program (PGEAS).

### Lattes Resume
 - http://lattes.cnpq.br/0286204782506578

### Research Gate contact
 - https://www.researchgate.net/profile/Alexandre_Mello5
