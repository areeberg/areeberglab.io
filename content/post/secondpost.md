---
title: Selecting Support Vector Candidates
date: 2017-07-04
---

This is my second post, how exciting!#

---
title: Code Sample
subtitle: Using Hugo or Pygments
date: 2016-03-08
tags: ["example", "code"]
---

The following are two code samples using syntax highlighting.

<!--more-->

The following is a code sample using triple backticks ( ``` ) code fencing provided in Hugo. This is client side highlighting and does not require any special installation.

```matlab
 function target = assigntarget(targetx,text1,text2)

vecsize=size(targetx);
for i=1:vecsize(1)
if(strcmp(targetx(i),text1)==1)
target(i)=1;
else
target(i)=-1;    

end

end

```


The following is a code sample using the "highlight" shortcode provided in Hugo. This is server side highlighting and requires Python and Pygments to be installed.

{{< highlight html >}}
function target = assigntarget(targetx,text1,text2)

vecsize=size(targetx);
for i=1:vecsize(1)
if(strcmp(targetx(i),text1)==1)
target(i)=1;
else
target(i)=-1;    

end

end

{{</ highlight >}}


And here is the same code with line numbers:

{{< highlight html "linenos=inline">}}
function target = assigntarget(targetx,text1,text2)

vecsize=size(targetx);
for i=1:vecsize(1)
if(strcmp(targetx(i),text1)==1)
target(i)=1;
else
target(i)=-1;    

end

end

{{</ highlight >}}
